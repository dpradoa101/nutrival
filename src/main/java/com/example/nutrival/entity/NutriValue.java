package com.example.nutrival.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class NutriValue {
  public static final long SERIALVERSIONUID = 1L;

  //  private String type;
  //  private Value value;

  private String name;
  private String calories;
  private String servingSizeG;
  private String fatTotalG;
  private String fatSaturatedG;
  private String proteinG;
  private String sodiumMg;
  private String potassiumMg;
  private String cholesterolMg;
  private String carbohydratesTotalG;
  private String fiberG;
  private String sugarG;

  //  @Override
  //  public String toString() {
  //    return "NutriValue{" + "type='" + type + '\'' + ", value=" + value + '}';
  //  }
}
